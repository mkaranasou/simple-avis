package gr.unipi.simpleavis;

import gr.unipi.simpleavis.utilities.ConnectionDetector;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
 

public class SimpleAvisMainActivity extends Activity {

	Button btnLogin;
	Button btnClear;
	EditText editTextUsername;
	EditText editTextPassword;
	ProgressDialog pDialog;
	TextView errorLbl;
	
	//Twitter - related variables
	static String TWITTER_CONSUMER_KEY = "sJycp7Rc5dW3fCv6XOdtfQ";
    static String TWITTER_CONSUMER_SECRET = "yGZPW40594Ni9HiwN5wkfjLVBfgAXgwr9es1sPts";
 
    // Preference Constants
    static String PREFERENCE_NAME = "twitter_oauth";
    static final String PREF_KEY_OAUTH_TOKEN = "oauth_token";
    static final String PREF_KEY_OAUTH_SECRET = "oauth_token_secret";
    static final String PREF_KEY_TWITTER_LOGIN = "isTwitterLogedIn";
	
    static final String TWITTER_CALLBACK_URL = "oauth://t4jsample";
    
    // Twitter oauth urls
    static final String URL_TWITTER_AUTH = "https://api.twitter.com/oauth/request_token";
    static final String URL_TWITTER_OAUTH_VERIFIER = "https://api.twitter.com/oauth/authorize";
    static final String URL_TWITTER_OAUTH_TOKEN = "https://api.twitter.com/oauth/access_token";
    // Twitter
    private static Twitter twitter;
    private static RequestToken requestToken;
 
    // Shared Preferences
    private static SharedPreferences mSharedPreferences;
 
    // Internet Connection detector
    private ConnectionDetector cd;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_simple_avis_main);
		checkConnection();

		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.simple_avis_main, menu);
		return true;
		

	}
	
	public void onLoginClick(View view){
		        
			       try {		        	
			        	requestToken = twitter.getOAuthRequestToken(TWITTER_CALLBACK_URL);
			        	this.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(requestToken.getAuthenticationURL())));
			        	} catch (TwitterException e) {
			        	e.printStackTrace();}
				

		
	}

	private void checkConnection(){
		cd = new ConnectionDetector(this);	
		// must Use a thread for network operation 
		this.runOnUiThread(new Runnable() {
					
				public void run() {
					 // Check if Internet present
					
						if (!cd.isConnectingToInternet()) {
							Toast.makeText(getBaseContext(), "No Internet Connection!!!Please, check and try again!", Toast.LENGTH_LONG).show();
        	
							}
						else
							{
								Toast.makeText(getApplicationContext(), "We have Internet Connection!!!", Toast.LENGTH_LONG).show();
        	
							}					
				}					
				});
	}
}
